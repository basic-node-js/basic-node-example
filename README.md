# Basic Node Example
Basic Example for NodeJS with Express, Sequelize, JWT, MySQL and Jade

- Express framework
- APIs for create user and login
- Jade template engine
- Using Sequelize for better model structure
- JWT for verifying login users
- Request validation by express-validator

## Getting started

```
git clone https://gitlab.com/basic-node-js/basic-node-example.git
```

## Installation

Follow below steps to install.
- Define your DB configuration and SECRET :

```textile
config/config.json
.env
```


For DB Migration:
```bash
$ npx sequelize-cli db:migrate
```

On MacOS or Linux, run the app with this command:
```bash
$ DEBUG=myapp:* npm start
```
On Windows Command Prompt, use this command:
```bash
> set DEBUG=myapp:* & npm start
```

On Windows PowerShell, use this command:
```bash
PS> $env:DEBUG='myapp:*'; npm start
```

## Usage

Go To: 
```bash
http://localhost:3000/
```

Create Users: 
```bash
POST: http://localhost:3000/users/create
{
    "first_name": "f1",
    "last_name": "l1",
    "email": "test@email.com",
    "password": "12345" 
}
```


Login User: 
```bash
POST: http://localhost:3000/users/login
{
    "email": "test@email.com",
    "password": "12345" 
}
```


Logged User detail: 
```bash
GET: http://localhost:3000/users/me
Header: authorization:Bearer {token}
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)