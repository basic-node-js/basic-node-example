var express = require('express');
var router = express.Router();
const Models = require('./../models');
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const User = Models.User;
const { body, validationResult } = require('express-validator');

dotenv.config();


router.post('/create',
  body('first_name').trim().escape(),
  body('last_name').trim().escape(),
  body('email').normalizeEmail().isEmail().custom(async (email) => {
    const existingUser =
      await User.findOne({ where: { email: email } });
    if (existingUser) {
      throw new Error('Email already in use')
    }
  }),
  body('password').isLength({ min: 5 }),
  async (req, res, next) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const salt = await bcrypt.genSalt(10);

    var usr = {
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email,
      password: await bcrypt.hash(req.body.password, 10)
    };
    created_user = await User.create(usr);
    res.status(201).json(created_user);
  });


router.post('/login',
  body('email').isEmail(),
  body('password').isLength({ min: 5 }),
  async (req, res, next) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const user = await User.findOne({ where: { email: req.body.email } });
    if (user) {
      const password_valid = await bcrypt.compare(req.body.password, user.password);
      if (password_valid) {
        token = jwt.sign({ "id": user.id, "email": user.email, "first_name": user.first_name }, process.env.SECRET);
        res.status(200).json({ token: token });
      } else {
        res.status(400).json({ error: "Password Incorrect" });
      }

    } else {
      res.status(404).json({ error: "User does not exist" });
    }

  });


/*** Get logged in user detail ***/
router.get('/me',
  async (req, res, next) => {
    try {
      let token = req.headers['authorization'].split(" ")[1];
      let decoded = jwt.verify(token, process.env.SECRET);
      req.user = decoded;
      next();
    } catch (err) {
      res.status(401).json({ "msg": "Couldnt Authenticate" });
    }
  },
  async (req, res, next) => {
    let user = await User.findOne({ where: { id: req.user.id }, attributes: { exclude: ["password"] } });
    if (user === null) {
      res.status(404).json({ 'msg': "User not found" });
    }
    res.status(200).json(user);
  });


module.exports = router;